﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Final_Libre_ChapoJulio
{
    public class Venta
    {
        public int CodigoVenta { get; set; }

        public DateTime FechaCompra { get; set; }
        public DetalleCompra Detalle { get; set; }
        public double ImporteTotal { get; set; }

        public Venta()
        {
            Principal principal = new Principal();
            CodigoVenta = principal.GenerarCodigoVenta();
 
        }

    }
}
