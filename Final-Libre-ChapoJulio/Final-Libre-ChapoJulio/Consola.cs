﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Final_Libre_ChapoJulio
{
    public class Consola: Dispositivo
    {
        public int CantidadControles { get; set; }
        public int CantidadJuegos { get; set; }

        public override double CalcularPrecioTotal()
        {

            if (GarantiaExtendida == garantia.Doce)
            {
 
                 return PrecioUnitario * 0.18;

            }
            if (GarantiaExtendida == garantia.Veinticuatro)
            {
                if (PrecioUnitario > 100000)
                {
                    return PrecioUnitario * 0.15;
                }
                return PrecioUnitario * 0.13;
            }
            if (GarantiaExtendida == garantia.Treintiseis)
            {
                return PrecioUnitario * 0.15;
            }
            return 0;
        }
    }
}
