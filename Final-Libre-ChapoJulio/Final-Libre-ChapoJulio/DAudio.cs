﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Final_Libre_ChapoJulio
{
    public class DAudio: Dispositivo
    {
        public double Decibeles { get; set; }
        public bool EsBluetooth { get; set; }


        public override double CalcularPrecioTotal()
        {

            if (GarantiaExtendida == garantia.Doce)
            {

                    return PrecioUnitario * 0.15;
            }
            if (GarantiaExtendida == garantia.Veinticuatro)
            {
                if (PrecioUnitario > 100000)
                {
                    return PrecioUnitario * 0.15;
                }
                return PrecioUnitario * 0.13;
            }
            if (GarantiaExtendida == garantia.Treintiseis)
            {
                return PrecioUnitario * 0.12;
            }
            return 0;
        }
    }
}
