﻿namespace Final_Libre_ChapoJulio
{
    public class Celular: Dispositivo
    {
        public double TamañoMemoriaPrincipal { get; set; }
        public double TamañoMemoriaRam { get; set; }
        public double TamañoPantalla { get; set; }
        public double CapacidadBateria { get; set; }

        //NO RECIBE EL PARAMETRO DE LA GARANTIA SELECCIONADA
        //EN TODAS LAS MULTIPLICACIONES, NO ES 0. SINO 1. PARA SUMARLO AL PRECIO, ASI SOLO DEVUELVE EL VALOR DE LA GARANTIA Y NO EL VALOR TOTAL
        public override double CalcularPrecioTotal()
        {

            if (GarantiaExtendida == garantia.Doce)
            {
                return PrecioUnitario * 0.15; 
            }

            if (GarantiaExtendida == garantia.Veinticuatro)
            {
                //ESTA PREGUNTA PODRIA HACERSE MEJOR JUNTO CON LA DEL EXCEL
                if (PrecioUnitario > 100000 && TamañoMemoriaRam > 3)
                {
                    return PrecioUnitario * 0.17;
                }
                else
                {
                    if (TamañoMemoriaRam > 3)
                    {
                        return PrecioUnitario * 0.15;
                    }
                }

                return PrecioUnitario * 0.13;
            }
            
            if (GarantiaExtendida == garantia.Treintiseis)
            {
                if (TamañoMemoriaRam> 4)
                {
                    return PrecioUnitario * 0.16;
                }
                return PrecioUnitario * 0.12;
            }
            
            return 0;
        }
    }
}