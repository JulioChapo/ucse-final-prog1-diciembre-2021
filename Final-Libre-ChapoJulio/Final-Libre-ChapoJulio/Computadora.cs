﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Final_Libre_ChapoJulio
{
    public class Computadora: Dispositivo
    {
        public bool EsNotebook { get; set; }
        public double CapacidadDiscoRigido { get; set; }
        public double CapacidadMemoria { get; set; }


        public override double CalcularPrecioTotal()
        {
       
            if (GarantiaExtendida == garantia.Doce)
            {
                if (EsNotebook == true)
                {
                    return PrecioUnitario * 0.19;
                }
                else
                    return PrecioUnitario * 0.15;
            }
            if (GarantiaExtendida == garantia.Veinticuatro)
            {
                if (PrecioUnitario > 100000)
                {
                    return PrecioUnitario * 0.15;
                }
                return PrecioUnitario * 0.13;
            }
            if (GarantiaExtendida == garantia.Treintiseis)
            {
                return PrecioUnitario * 0.12;
            }
            return 0;
        }
    }
}
