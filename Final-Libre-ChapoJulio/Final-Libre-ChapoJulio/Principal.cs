﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Final_Libre_ChapoJulio
{
    public class Principal
    {
        List<Dispositivo> Dispositivos = new List<Dispositivo>();
        List<int> Codigo = new List<int> { 999 }; //PUEDE SER UN VALOR NUMERICO DIRECTAMENTE.
        List<Venta> Ventas = new List<Venta>();

        public int GenerarCodigo()
        {
            return Codigo.LastOrDefault() + 1;
        }

        public int GenerarCodigoVenta()
        {
            return Ventas.Count() + 1;
        }

        public void VenderProducto(Dispositivo dispositivoComprado)
        {
            Venta nuevaVenta = new Venta();
            nuevaVenta.FechaCompra = DateTime.Today;
            //DETALLE ES NULL, EN EJECUCION DA UNA EXCEPCION
            nuevaVenta.Detalle.FechaInicio = DateTime.Today;

            //LA VIGENCIA ES ESTA SOLO SI NO COMPRA GARANTIA EXTENDIDA, SINO TENDRIA QUE SUMARSE LOS MESES DE GARANTIA EXTENDIDA
            nuevaVenta.Detalle.FechaFin = nuevaVenta.Detalle.FechaInicio.AddMonths(dispositivoComprado.CantMesesGarantia);
            nuevaVenta.Detalle.CodigoProductoComprado = dispositivoComprado.CodigoDispositivo;
            nuevaVenta.Detalle.CantMesesGarantia = dispositivoComprado.CantMesesGarantia;
            
            //ESTE NO ES UN VALOR DEL DISPOSITIVO, DEBERIA RECIBIRLO COMO UN PARAMETROS DISTINTO A DISPOSITIVO, Y OPCIONAL
            nuevaVenta.Detalle.GarantiaExtendida = (DetalleCompra.garantia)dispositivoComprado.GarantiaExtendida;
            
            nuevaVenta.ImporteTotal = dispositivoComprado.CalcularPrecioTotal();
            
            Ventas.Add(nuevaVenta);
        }

        //ESTOS DOS METODOS DE ABAJO PUEDEN SER UNO SOLO Y COMBINARLOS, SINO SE DUPLICA CODIGO
        public List<int> FiltrarDispositivosPorPrecio(double precioMin, double precioMax)
        {
            List<int> listaPrecios = new List<int>();
            foreach (var dispositivo in Dispositivos)
            {
                if (dispositivo.PrecioUnitario >= precioMin && dispositivo.PrecioUnitario <= precioMax)
                {
                    listaPrecios.Add(dispositivo.CodigoDispositivo);
                }
            }
            return listaPrecios;
        }

        public List<int> FiltrarPorColor(string color)
        {
            int aux = 0;
            List<int> FiltroColor = new List<int>();
            foreach (var dispositivo in Dispositivos)
            {
                aux = dispositivo.FiltrarPorColor(color);
                if (aux != 0)
                {
                    FiltroColor.Add(aux);
                }
            }
            return FiltroColor;
        }
        public List<string> FiltrarPorGarantia(DateTime fechaInicio, DateTime fechaFin)
        {
            foreach (var venta in Ventas)
            {
                if (venta.Detalle.GarantiaExtendida <= (venta.FechaCompra && detalleCompra.GarantiaExtendida != DetalleCompra.garantia.Ninguna)
                {

                }
                
            }
        }

    


    }
}
