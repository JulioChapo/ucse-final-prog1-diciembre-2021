﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Final_Libre_ChapoJulio
{
    public abstract class Dispositivo
    {
        public int CodigoDispositivo { get; set; }
        public string Nombre { get; set; }
        public string Color { get; set; }
        public int CantMesesGarantia { get; set; }
        public double PrecioUnitario { get; set; }
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public DateTime FechaFabricacion { get; set; }
        public garantia GarantiaExtendida { get; set; } //ESTO NO ES DEL PRODUCTO, ES DE LA VENTA.
        public enum garantia
        {
            Ninguna, //ESTO NO ES NECESARIO. SI NO HAY GARANTIA DEBERIA SER NULL
            Doce,
            Veinticuatro,
            Treintiseis
        }

        public Dispositivo() //ERROR, NO SE DEBE INSTANCIAR PRINCIPAL DESDE LAS OTRAS CLASES. PODRIA USAR UN PARAMETRO EN EL CONSTRUCTOR
        {
            Principal principal = new Principal();
            CodigoDispositivo = principal.GenerarCodigo();
        }


        public abstract double CalcularPrecioTotal();

        public int FiltrarPorColor(string color)
        {
            if (Color == color)
            {
                return CodigoDispositivo;
            }
            return 0;
        }
    }
}
