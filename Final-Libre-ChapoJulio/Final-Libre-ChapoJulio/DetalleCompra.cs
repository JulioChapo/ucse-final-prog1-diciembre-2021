﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Final_Libre_ChapoJulio
{
    public class DetalleCompra
    {
        public int CodigoProductoComprado { get; set; }
        public double ImporteProducto { get; set; }
        public int CantMesesGarantia { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFin { get; set; }
        public garantia GarantiaExtendida { get; set; }

        public enum garantia
        {
            Ninguna, //NO ES NECESARIO ,LA PROPIEDAD PUEDE SER NULL
            Doce,
            Veinticuatro,
            Treintiseis
        }



    }
}
